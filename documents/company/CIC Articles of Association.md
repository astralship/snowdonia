##
# **Articles of Association**

## **Companies Act 2006 Private Company Limited by Guarantee**

## **Articles of Astralship Collective CIC**

##
# **Section One**

##
# **Interpretations &amp; Definitions**

1. In these Articles:

&quot; **Address&quot;** means a postal address or, for the purposes of electronic communication, a fax number, email address or telephone number for receiving text messages;

&quot; **Articles&quot;** means the Co-operative&#39;s articles of association;

 &quot; **Astralship**&quot; means a property, site or facility operated by the CIC who purpose is to be an enhanced environment for member of the Collective to work toward their aim more effectively. These will generally have residential facilities.

&quot; **The Board of Directors&quot;** or &quot; **Board&quot;** means all those persons appointed to perform the duties of directors of the Company;

&quot; **Companies Acts&quot;** or **&quot;the Act&quot;** means the Companies Acts (as defined in section 2 of the Companies Act 2006) in so far as they apply to the company;

&quot; **Director&quot;** means a director of the Co-operative and includes any person occupying the position of Director, by whatever name called;

&quot; **Document&quot;** includes, unless otherwise stated, any document sent or supplied in electronic form;

&quot; **Electronic means**&quot; has the meaning given in section 1168 of the Companies Act 2006;

&quot; **Employee&quot;** means anyone over the age of 16 holding a contract of employment with the company.

&quot; **Entrenched&quot;** has the meaning given by section 22 of the Companies Act 2006 and as detailed under the heading &#39;resolutions&#39; in these Articles;

&quot; **Member** has the same meaning given in section 112 of the Companies Act 2006 to &#39;&#39;Member&#39;&#39;, as detailed under &#39;Membership&#39; in these Articles;

&quot; **Regulations&quot;** has the meaning as detailed under &#39;Regulations&#39; in these Articles;

&quot; **Secretary&quot;** means any person appointed to perform the duties of the Secretary of the Cooperative;

&quot; **Writing&quot;** means the representation or reproduction of words, symbols or other information in a visible form by any method or combination of methods, whether sent or supplied in electronic form or otherwise.

Unless the context requires otherwise, other words or expressions contained in these Articles bear the same meaning as in the Companies Act 2006 as in force on the date when these Articles become binding on the company.

##
# **Section Two**

##
# **Purpose &amp; Objects**

2. The name of the Company is: Astralship Collective CIC.

3. The registered office of the Company is at:

4. The company is to be a Community Interest Company:

1. Its objects are to carry on any industry, business or trade which benefit the community and in particular in order to establish novel facilitated, residential co-living co-working spaces where (insert definition) will be able to live some or all of the time, having their essential needs met and be able to apply their talents to creating real value for humanity in an optimised way. With an eye to considering and developing solutions to grand societal, technological and environmental challenges. Wherever the above mentioned spaces are established the CIC will seek to directly or indirectly support local residents, whether or not they are CIC members, through local activities in particular by carrying out projects that support local community education and wellbeing.

# **Section Three**

# **Membership**

5. The subscribers to the Memorandum are the first members of the Company.

6: Such other persons as are admitted to membership in accordance with the Articles shall be members of the Company.

7: No person shall be admitted a member of the Company unless he or she is approved by the Directors.

8: Every person who wishes to become a member shall deliver to the company an application for membership in such form (and containing such information) as the Directors require and executed by him or her.

9: Membership is not transferable to anyone else.

10: Each person who is a member will be bound by their agreement to be a member, to pay £1 towards settling the debts of the company upon its dissolution, if such debts exist.

**11. Cancellation of Membership:**

a) Membership will be terminated under the following circumstances:

i. at the discretion of the board;

ii. on receipt of a written request by a member for the cancellation of their own

membership;

iii. if, over a period of two years, reasonable attempts to communicate with the

member (including a written warning that membership may be cancelled if no

response is received) elicit no response;

iv. if the member dies or ceases to exist;

v. or otherwise according to the Articles.

**12. Register of members:**

1. A register of members is kept at the registered office, and will include:

i. the name of every member;

ii. the address and other contact details, and whether electronic communications

are to be used;

iii. The class of membership for each member;

iv. any loans or other property held by members;

v. the date on which the member&#39;s name was entered on the register, and the

date on which they ceased to be members;

vi. the names of directors and officers of the Company, the positions held by

them, and the dates on which those appointments began and ended.

a) While a member shall be allowed on reasonable notice to inspect extracts

from the register, names in (i) and financial data in (iv) will only be linked to other

data in accordance with privacy law and best practice.

# **Section Four**

# **Governance**

**13. General Meetings.**

a) The board of directors may at any point they wish convene a general meeting.

b) General meetings must be held in accordance with the provisions regarding such meetings in the Companies Act.

c) A person who is not a member of the Company shall not have any right to vote at a general meeting of the Company; but this is without prejudice to any right to vote on a resolution affecting the rights attached to a class of the Company&#39;s debentures.

d) article 13. (c) shall not prevent a person who is a duly appointed proxy for a member or a duly authorised representative of a member from voting at a general meeting of the Company.

e) Voting at general meetings shall be on the basis of one member, one vote.

f) Where a member is an organisation (incorporated or unincorporated), its rights will be exercised by a person who is nominated by that organisation in accordance with their rules. That person will cease to do so if for any reason that nomination is no longer sustained by the member organisation.

g) No business will be transacted at any general meeting unless a quorum is present. A quorum is present if 50% of the membership either in person or represented by proxy are present at the designated start of the general meeting.

h) Members of the (........) may pass though a simple majority vote resolutions at a general meeting.

i) A resolution shall be considered passed when a majority of the members present at the general meeting have signified their agreement to it.

**14. Members Reserve Power**

a) The members may, by special resolution, direct the Directors to take, or refrain from taking, specific action.

b) No such special resolution invalidates anything which the Directors have done before the passing of the resolution.

**15. Written Resolutions**

a) Resolutions may in addition to standard resolutions be passed at a general meetings as written resolutions.

b) A written resolution passed by Members shall be effective if it has been passed in accordance with the requirements of the Act which includes sending a copy of the proposed resolution to every Member, together with a statement informing the member how to signify their agreement to the resolution and the date by which the resolution must be passed if it is not to lapse. Communications in relation to written notices shall be sent to the Company&#39;s auditors in accordance with the Companies Acts. Written resolutions may comprise several copies to which one or more Members have signified their agreement.

c) A written resolution shall be deemed to have been passed if, within 28 days of the written resolution&#39;s circulation date:

i) Written approval has been received from at least 75% of the Members where the resolution is a special resolution;
 ii) Written approval has been received from at least 51% of the Members where the resolution is an ordinary resolution.

d) In accordance with the Companies Acts, resolutions to remove a Director or auditor (or their equivalent) of the Cooperative before the end of their period of office shall not be passed by written resolution.

**16. Treasurer and Secretary**

a) The Company will have a secretary and a treasurer These positions may be appointed or replaced through a resolution passed at a general meeting. If a general meeting does not appoint them, the board of directors will be duty-bound to do so.

**17. Board of directors**

a) Subject to the Articles, the Directors are responsible for the management of the Company&#39;s business, for which purpose they may exercise all the powers of the Company.

**18. Chair**

a) The Directors may appoint one of their number to be the chair of the Directors for such term of office as they determine and may at any time remove him or her from office.

**19. Directors may delegate**

a) Subject to the Articles, the Directors may delegate any of the powers which are conferred on them under the Articles or the implementation of their decisions or day to day management of the affairs of the Company:

to such person or committee;

by such means (including by power of attorney);

to such an extent;

in relation to such matters or territories; and

on such terms and conditions;

as they think fit.

b) If the Directors so specify, any such delegation of this power may authorise further delegation of the Directors&#39; powers by any person to whom they are delegated.

c) The Directors may revoke any delegation in whole or part, or alter its terms and conditions.

**20. Decision Making**

a) Questions arising at a Directors&#39; meeting shall be decided by a majority of votes

b) Questions arising at a Directors meeting may be taken in accordance with Article 24., subject to the limitations of such as detailed in Article 24.

c) In all proceedings of Directors each Director must not have more than one vote

**21. Calling a meeting of the board**

a) Directors may (and the Secretary, if any, must at the request of two Directors) call a Directors&#39; meeting.

b) A Directors&#39; meeting must be called by at least seven Clear Days&#39; notice unless either:

i. all the Directors agree; or

ii. urgent circumstances require shorter notice.

c) Notice of Directors&#39; meetings must be given to each Director.

d) Every notice calling a Directors&#39; meeting must specify:

i. the place, day and time of the meeting; and

ii. if it is anticipated that Directors participating in the meeting will not be in the same place, how it is proposed that they should communicate with each other during the meeting.

e) Notice of Directors&#39; meetings need not be in Writing.

f) Notice of Directors&#39; meetings may be sent by Electronic Means to an Address provided by the Director for the purpose.

**22. Quorum for Directors&#39; meetings**

a) At a Directors&#39; meeting, unless a quorum is participating, no proposal is to be voted on, except a proposal to call another meeting.

b) The quorum for Directors&#39; meetings may be fixed from time to time by a decision of the Directors, but it must never be less than two, and unless otherwise fixed it is [two].

c) If the total number of Directors for the time being is less than the quorum required, the Directors must not take any decision other than a decision:

i. to appoint further Directors; or

ii. to call a general meeting so as to enable the members to appoint further Directors.

**23. Chairing of Directors&#39; meetings**

a) The Chair, if any, or in his or her absence another Director nominated by the Directors present shall preside as chair of each Directors&#39; meeting.

**24. Decisions without a meeting**

a) The Directors may take a unanimous decision without a Directors&#39; meeting in accordance with this Article by indicating to each other by any means, including without limitation by Electronic Means, that they share a common view on a matter. Such a decision may, but need not, take the form of a resolution in Writing, copies of which have been signed by each Director or to which each Director has otherwise indicated agreement in Writing.

b) A decision which is made in accordance with Article 24. shall be as valid and effectual as if it had been passed at a meeting duly convened and held, provided the following conditions are complied with:

i. approval from each Director must be received by one person being either such person as all the Directors have nominated in advance for that purpose or such other person as volunteers if necessary (&quot;the Recipient&quot;), which person may, for the avoidance of doubt, be one of the Directors;

ii. following receipt of responses from all of the Directors, the Recipient must communicate to all of the Directors by any means whether the resolution has been formally approved by the Directors in accordance with this Article 24. (b).;

iii. the date of the decision shall be the date of the communication from the Recipient confirming formal approval;

ix. the Recipient must prepare a minute of the decision in accordance with Article 36.

**25. Conflicts of interest**

a) Whenever a Director finds himself or herself in a situation that is reasonably likely to give rise to a Conflict of Interest, he or she must declare his or her interest to the Directors unless, or except to the extent that, the other Directors are or ought reasonably to be aware of it already.

b) If any question arises as to whether a Director has a Conflict of Interest, the question shall be decided by a majority decision of the other Directors.

c) Whenever a matter is to be discussed at a meeting or decided in accordance with Article 24 and a Director has a Conflict of Interest in respect of that matter then, subject to Article 26, he or she must:

i. remain only for such part of the meeting as in the view of the other Directors is necessary to inform the debate;

ii. not be counted in the quorum for that part of the meeting; and

iii. withdraw during the vote and have no vote on the matter.

d) When a Director has a Conflict of Interest which he or she has declared to the Directors, he or she shall not be in breach of his or her duties to the Company by withholding confidential information from the Company if to disclose it would result in a breach of any other duty or obligation of confidence owed by him or her.

**26. Directors&#39; power to authorise a conflict of interest**

a) The Directors have power to authorise a Director to be in a position of Conflict of Interest provided:

i. in relation to the decision to authorise a Conflict of Interest, the conflicted Director must comply with Article 25. (c);

ii. in authorising a Conflict of Interest, the Directors can decide the manner in which the Conflict of Interest may be dealt with and, for the avoidance of doubt, they can decide that the Director with a Conflict of Interest can participate in a vote on the matter and can be counted in the quorum;

iii. the decision to authorise a Conflict of Interest can impose such terms as the Directors think fit and is subject always to their right to vary or terminate the authorisation.

b) If a matter, or office, employment or position, has been authorised by the Directors in accordance with Article 26. (a) then, even if he or she has been authorised to remain at the meeting by the other Directors, the Director may absent himself or herself from meetings of the Directors at which anything relating to that matter, or that office, employment or position, will or may be discussed.

c) A Director shall not be accountable to the Company for any benefit which he or she derives from any matter, or from any office, employment or position, which has been authorised by the Directors in accordance with Article 26. (a) (subject to any limits or conditions to which such approval was subject).

**27. Register of Directors&#39; interests**

a) The Directors shall cause a register of Directors&#39; interests to be kept. A Director must declare the nature and extent of any interest, direct or indirect, which he or she has in a proposed transaction or arrangement with the Company or in any transaction or arrangement entered into by the Company which has not previously been declared.

**28. Appointment of Directors**

a) Those persons notified to the Registrar of Companies as the first Directors of the Company shall be the first Directors.

b) Any person who is a member of the (insert name), willing to act as a Director, and is permitted by law to do so, may be appointed to be a Director by a decision of the Directors.

**29. Termination of Director&#39;s appointment**

a) A person ceases to be a Director as soon as:

i. that person ceases to be a Director by virtue of any provision of the Companies Act 2006, or is prohibited from being a Director by law;

ii. a bankruptcy order is made against that person, or an order is made against that person in individual insolvency proceedings in a jurisdiction other than England and Wales or Northern Ireland which have an effect similar to that of bankruptcy;

iii. a composition is made with that person&#39;s creditors generally in satisfaction of that person&#39;s debts;

ix. notification is received by the Company from the Director that the Director is resigning from office, and such resignation has taken effect in accordance with its terms (but only if at least two Directors will remain in office when such resignation has taken effect); or

v. the Director fails to attend three consecutive meetings of the Directors and the Directors resolve that the Director be removed for this reason.

vi. the Director ceases to be a member.

**30. Directors&#39; remuneration**

a) Directors may undertake any services for the Company that the Directors decide.

b) Directors are entitled to such remuneration as the Directors determine:

i. for their services to the Company as Directors; and

ii. for any other service which they undertake for the Company.

c) Subject to the Articles, a Director&#39;s remuneration may:

i. take any form; and

ii. include any arrangements in connection with the payment of a pension, allowance or gratuity, or any death, sickness or disability benefits, to or in respect of that director.

d) Unless the Directors decide otherwise, Directors&#39; remuneration accrues from day to day.

e) Unless the Directors decide otherwise, Directors are not accountable to the Company for any remuneration which they receive as Directors or other officers or employees of the Company&#39;s subsidiaries or of any other body corporate in which the Company is interested.

**31. Directors&#39; expenses**

a) The Company may pay any reasonable expenses which the Directors properly incur in connection with their attendance at:

i. meetings of Directors or committees of Directors;

ii. general meetings; or

iii. separate meetings of any class of members or of the holders of any debentures of the Company,

or otherwise in connection with the exercise of their powers and the discharge of their responsibilities in relation to the Company.

# **Section Five**

# **Application of Profits**

**32. Not for profit**

a) The Company is not established or conducted for private gain: any surplus or assets are used principally for the benefit of the community.

b) The Company within the limitations imposed above has the general aim of creating common wealth, building an indivisible reserve and providing a return on investment no more than is necessary to attract and retain the capital it requires to remain functional.

**33. Asset Lock**

a) The Company shall not transfer any of its assets other than for full consideration.

b) Provided the conditions in article 32 (b) are satisfied, article 32 (a) shall not apply to:

i. the transfer of assets to any specified asset-locked body, or (with the consent of the Regulator) to any other asset-locked body; and

ii. the transfer of assets made for the benefit of the community other than by way of a transfer of assets into an asset-locked body.

ii) The conditions are that the transfer of assets must comply with any restrictions on the transfer of assets for less than full consideration which may be set out elsewhere in the Memorandum or Articles of the Company.

**34. Division on dissolution:**

a) In the event of the winding up or dissolution of the Company the assets of the Company will first, according to law, be used to satisfy its debts and liabilities.

b) The remaining assets will be transferred to a common ownership enterprise that has objects consistent with the objects of the Company stated in article 4. above, subject to any restrictions in 32. above, as may be nominated by the members at the time of or prior to the dissolution. If no such organisation is nominated, the assets will be transferred to

……………………………………………………

c) In the event that for whatever reason any residual assets cannot be transferred as described above, they will be given for charitable purposes. The Regulators consent will be sought before any such transfer is made. No amendment will be made that would reduce the amount given to social and charitable purposes, or remove this sentence.

## **Section Eight**

## **Administrative Arrangements**

**35. Provision of information**

a) A copy of these Articles, and any amendments made to them, will be given free of charge to every member on admission or on request. The board will accommodate any reasonable request to explain or clarify the meaning of the Articles, and justify its interpretation of them.

b) The following information will be advertised in the notice of all general meetings, with advice on how full copies may be obtained promptly and without charge:

i. standing orders relating to the meeting in question;

ii. a list of key decisions taken or consulted on since the last AGM, and how they were resolved;

iii. a guide to the rights and responsibilities of directors and the process for members to put themselves forward for election;

iv. details of other ways for members to participate in the governance of the Company;

v. a list of policy documents covering standards and procedures that apply to the full range of the Company&#39;s activities.

c) The following information will be recorded, retained and made available at no charge to members:

i. agendas, minutes and papers presented to general meetings;

ii. quarterly management accounts (unless the annual turnover of the Company is below £25,000);

iii. annual returns submitted to the FCA, HMRC and other federal or governmental bodies;

iv. responses to statutory consultations made by the Company;

v. job descriptions and line management of any staff employed, and statistics relating to staff disputes and grievances, workplace injuries and staff retention.

d) No information will be provided to a member or any other person, or made available for general viewing, that would disclose details of the financial transactions of another member with the Company, other than with their permission. If the board refuses a request for information, it must explain what reason it has for withholding the information.

e) The board may redact portions of documents provided to members for reasons of commercial confidentiality or personal privacy provided it is clear and where this has been done.

**36. Means of communication to be used**

a) Subject to the Articles, anything sent or supplied by or to the Company under the Articles may be sent or supplied in any way in which the Companies Act 2006 provides for Documents or information which are authorised or required by any provision of that Act to be sent or supplied by or to the Company.

b) Subject to the Articles, any notice or Document to be sent or supplied to a Director in connection with the taking of decisions by Directors may also be sent or supplied by the means by which that Director has asked to be sent or supplied with such notices or Documents for the time being.

c) A Director may agree with the Company that notices or Documents sent to that Director in a particular way are to be deemed to have been received within an agreed time of their being sent, and for the agreed time to be less than 48 hours.

Irregularities.

d) The proceedings at any meeting or on the taking of any poll or the passing of a written resolution or the making of any decision shall not be invalidated by reason of any accidental informality or irregularity (including any accidental omission to give or any non-receipt of notice) or any want of qualification in any of the persons present or voting or by reason of any business being considered which is not referred to in the notice unless a provision of the Companies Acts specifies that such informality, irregularity or want of qualification shall invalidate it.

**37. Minutes**

a) The Directors must cause minutes to be made in books kept for the purpose:

i. of all appointments of officers made by the Directors;

ii. of all resolutions of the Company and of the Directors (including, without limitation, decisions of the Directors made without a meeting); and

iii. of all proceedings at meetings of the Company and of the Directors, and of committees of Directors, including the names of the Directors present at each such meeting;

iv. and any such minute, if purported to be signed (or in the case of minutes of Directors&#39; meetings signed or authenticated) by the chair of the meeting at which the proceedings were had, or by the chair of the next succeeding meeting, shall, as against any member or Director of the Company, be sufficient evidence of the proceedings.

b) The minutes must be kept for at least ten years from the date of the meeting, resolution or decision.

**38. Records and accounts**

a) The Directors shall comply with the requirements of the Companies Acts as to maintaining a members&#39; register, keeping financial records, the audit or examination of accounts and the preparation and transmission to the Registrar of Companies and the Regulator of:

i. annual reports;

ii. annual returns; and

iii. annual statements of account.

b) Except as provided by law or authorised by the Directors or an ordinary resolution of the Company, no person is entitled to inspect any of the Company&#39;s accounting or other records or Documents merely by virtue of being a member.

**39. Indemnity**

Subject to Article 39. (b), a relevant Director of the Company or an associated company may be indemnified out of the Company&#39;s assets against:

i. any liability incurred by that Director in connection with any negligence, default, breach of duty or breach of trust in relation to the Company or an associated company;

any liability incurred by that Director in connection with the activities of the Company or an associated company in its capacity as a trustee of an occupational pension scheme (as defined in section 235(6) of the Companies Act 2006); and

ii. any other liability incurred by that Director as an officer of the Company or an associated company.

b) This Article does not authorise any indemnity which would be prohibited or rendered void by any provision of the Companies Acts or by any other provision of law.

c) In this Article:

i. companies are associated if one is a subsidiary of the other or both are subsidiaries of the same body corporate; and

ii. a &quot;relevant Director&quot; means any Director or former Director of the Company or an associated company.

**40. Annual Return**

a) Every year, and in accordance with the requirements of the law, the Secretary will send the annual return relating to the Company&#39;s affairs for the required period to the Regulator.
